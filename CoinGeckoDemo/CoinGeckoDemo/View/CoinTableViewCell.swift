//
//  CoinTableViewCell.swift
//  CoinGeckoDemo
//
//  Created by Admin on 8/28/22.
//

import UIKit

class CoinTableViewCell: UITableViewCell {
    
    let coinImageView = UIImageView()
    let coinName = UILabel()
    let coinPrice = UILabel()
    let coinSymbol = UILabel()
    let coinRank = UILabel()
    let stackView = UIStackView()

    
    func coin(coin: CoinModel) {
        coinImageView.image = coin.image!.stringToImage()
        coinName.text = coin.name
        coinSymbol.text = coin.symbol
        coinPrice.text = "Price: $\(coin.current_price ?? 0.00)"
        coinRank.text = "Market Cap: \(coin.market_cap ?? 0)"
    }
    
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setStackView()
        labelsAndImageSetup()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    //MARK: - Stack View Setup
    func setStackView() {
        
        stackView.axis = NSLayoutConstraint.Axis.vertical
        stackView.alignment = .center
        stackView.distribution = .fill
        stackView.spacing = 10
        stackView.addArrangedSubview(coinImageView)
        stackView.addArrangedSubview(coinName)
        stackView.addArrangedSubview(coinSymbol)
        stackView.addArrangedSubview(coinPrice)
        stackView.addArrangedSubview(coinRank)
       
        
        stackView.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(stackView)
        let constraints = [stackView.topAnchor.constraint(equalTo: self.topAnchor,constant: 10),
                           stackView.centerYAnchor.constraint(equalTo: self.centerYAnchor),
                           stackView.leadingAnchor.constraint(equalTo: self.leadingAnchor , constant: 10),
                           stackView.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -10)
        ]
        NSLayoutConstraint.activate(constraints)
        
    }
    
    
    //MARK: - Labels Setup
    
    func labelsAndImageSetup() {
        /// image
        coinImageView.translatesAutoresizingMaskIntoConstraints = false
        coinImageView.heightAnchor.constraint(equalToConstant: self.frame.width / 10).isActive = true
        coinImageView.widthAnchor.constraint(equalToConstant: self.frame.width / 10).isActive = true
        
        /// labels
        coinName.numberOfLines = 0
        coinPrice.numberOfLines = 0
        coinRank.numberOfLines = 0
        coinPrice.font =  UIFont.boldSystemFont(ofSize: 18)
        coinSymbol.textColor = .gray
        
    }
    
}



