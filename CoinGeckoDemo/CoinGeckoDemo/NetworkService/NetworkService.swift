//
//  NetworkService.swift
//  CoinGeckoDemo
//
//  Created by Admin on 8/28/22.
//

import Foundation

final class NetworkService {
    
    static let shared = NetworkService()
    
    private init() {}
    
    func callApi<T:Decodable>(urlString: String, expect: T.Type, completion: @escaping (Result<T,Error>) -> ()) {
        
        guard let url = URL(string: urlString) else {
            return
        }
        
        URLSession.shared.dataTask(with: url) { data, _, error in
            
            if let error = error {
                
                print(error.localizedDescription)
            }else if let data = data {
                
                do {
                    let result = try JSONDecoder().decode(expect, from: data)
                    completion(.success(result))
                }catch {
                    completion(.failure(error))
                    print(error.localizedDescription)
                }
            }
        }.resume()
    }
    
}
