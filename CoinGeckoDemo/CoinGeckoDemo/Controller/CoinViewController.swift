//
//  CoinViewController.swift
//  CoinGeckoDemo
//
//  Created by Admin on 8/28/22.
//

import UIKit

class CoinViewController: UIViewController {
    
    var coinList = [CoinModel]()
    var tableView = UITableView()
    let urlString = "https://api.coingecko.com/api/v3/coins/markets?vs_currency=usd&order=market_cap_desc&per_page=100&page=1"

    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.addSubview(tableView)
        view.backgroundColor = .white
        
        tableViewConfigration()
        coinApiCall()
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        setTableViewConstraints()
      
    }
    
    //MARK: - Api Call
    
    func coinApiCall() {
        NetworkService.shared.callApi(urlString: urlString, expect: [CoinModel].self) { [weak self] result in
            switch result {
            case .success(let data):
                self?.coinList = data
                DispatchQueue.main.async {
                    self?.tableView.reloadData()
                }
                print(data.count)
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }
    
    //MARK: - TableView Setting
    func setTableViewConstraints() {
        
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.delegate = self
        tableView.dataSource = self
        let constraints = [tableView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
                           tableView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
                           tableView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor),
                           tableView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor)]
        NSLayoutConstraint.activate(constraints)
        
    }
    
    func tableViewConfigration() {
        
        tableView.register(CoinTableViewCell.self, forCellReuseIdentifier: "cell")
        tableView.delegate = self
        tableView.dataSource = self
        tableView.estimatedRowHeight = 80
        tableView.rowHeight = UITableView.automaticDimension
    }

}


//MARK: - Table View Delegates
extension CoinViewController: UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return coinList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! CoinTableViewCell
        let coin = coinList[indexPath.row]
        cell.coin(coin: coin)
        return cell
    }
    
}
