//
//  CoinModel.swift
//  CoinGeckoDemo
//
//  Created by Admin on 8/28/22.
//

import Foundation

struct CoinModel: Decodable {
    
    let name: String?
    let symbol: String?
    let current_price: Double?
    let image: String?
    let market_cap: Int?
    
}
