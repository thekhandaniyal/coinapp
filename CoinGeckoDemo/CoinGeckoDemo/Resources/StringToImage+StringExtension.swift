//
//  StringToImage+StringExtension.swift
//  CoinGeckoDemo
//
//  Created by Admin on 8/28/22.
//

import UIKit

extension String {
    
    func stringToImage() -> UIImage? {
        
        guard let urlImage = URL(string: self) else {
            return nil
        }
        do {
            let data = try Data(contentsOf: urlImage)
            return UIImage(data: data)
        }catch{
            print(error.localizedDescription)
        }
        return nil
    }
}
